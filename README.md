# real.vrchat world, forked from world-map-starterkit

## Aufbau

Unsere Welt ist in `real.json` definiert. Diese Datei kann in `Tiled` unter "Open File" geladen und bearbeitet werden. Dort sind einzelne Tilemaps eingebunden, unsere Tiles sind unter `tiles_png/unreal-vrchat`. Ich habe eine erste Tilemap dort angelegt und ein paar Tiles in die Welt kopiert. Testen unter der URL siehe unten.

## howto

### Workflow

1. `real.json` mit `Tiled` bearbeiten und Tilemaps mit `Krita` bearbeiten
2. Änderungen ins Repository puschen
3. Nach max. 10 Minuten kann die Welt getestet werden, Link siehe unten.

### Krita

Unter `Settings > Display > Pixel Grid` `Start showing at: 400%` einstellen, damit die Linien sichtbar werden. Zum Malen rechts unten in der Suche zur Pinselauswahl `Pixel` eingeben, um nur die für Pixel Art geeigneten Pinsel zu sehen.

### Titled

`json`-Datei unter `Open File` Laden.

Tiles aus Tilesets in die Welt kopieren geht so:

1. Oben `Brush` Tool auswählen
2. Rechts unter `Layers` ein `Tile Layer` auswählen oder ein neues anlegen.
3. Rechts unter `Tilesets` ein Tileset auswählen und dann darin die Tiles auswählen
4. Im Editor, in dem man die Welt in groß sieht, die ausgehählten Tiles aufstempeln


## link for c3worlds

Testing: https://test.visit.at.wa-test.rc3.cccv.de/_/global/retani.gitlab.io/real-vrchat-world-map/main.json

## Website

The second part of this map is the website that serves several assets. It is in the `website` folder and needs to be deployed to `https://real-vrchat-world-data.intergestalt.dev`

### dokku deployment

(dokku cli installed)


```
git remote add dokku dokku@intergestalt.dev:real-vrchat-world-data
git push dokku
dokku config:set NGINX_ROOT=website
git push dokku
git letsencrypt
```
